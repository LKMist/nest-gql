import { Injectable } from '@nestjs/common';
import { Prisma } from "./prisma.binding"

@Injectable()
export class PrismaService extends Prisma {
    constructor() {
        super({
            endpoint:
            "https://address-book-prisma.dev.welink.net.za/",
            secret: "Be11er$@!!"
        })
    }
}
