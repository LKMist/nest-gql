import { Module } from '@nestjs/common';
import {GraphQLModule} from "@nestjs/graphql"
import { PrismaModule } from './prisma/prisma.module';
import { MessagesModule } from './messages/messages.module';
import { UsersModule } from './users/users.module';


@Module({
  imports: [
    GraphQLModule.forRoot({ // This registers the GraphQLModule with the server. The .forRoot() method takes an options object as an argument.
      typePaths: ["./**/*.graphql"], // tells the GraphQLModule where in the project to look for GraphQL files.
      resolverValidationOptions: { // Resolves a temporary bug TODO: See if resolved and removeable.
        requireResolversForResolveType: false
      }
    }),
    MessagesModule,
    PrismaModule,
    UsersModule
  ],
})
export class AppModule {}
