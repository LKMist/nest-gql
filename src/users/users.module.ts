import { Module } from '@nestjs/common';
import { PrismaModule } from 'src/prisma/prisma.module';
import { UsersResolver } from "./users.resolver"


@Module({
  imports: [PrismaModule],
  providers: [UsersResolver],
  exports: [UsersResolver]
})
export class UsersModule {}
