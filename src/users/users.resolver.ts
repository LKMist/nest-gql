import {Query, Mutation, Resolver, Args} from "@nestjs/graphql"
import {PrismaService} from "../prisma/prisma.service"

@Resolver()
export class UsersResolver {
    constructor(private readonly prisma: PrismaService){}

    @Query()
    users(@Args() args) {
        console.log("UsersResolver -> users -> args", args)
        return this.prisma.query.users(args); // Forwarding the query on to the Prisma Service.
    }
    @Mutation()
    createUser(@Args() args) {
        console.log("UsersResolver -> createUser -> args", args)
        return this.prisma.mutation.createUser(args)
    }
    @Mutation()
    deleteUser(@Args() args) {
        return this.prisma.mutation.deleteUser(args)
    }
}