import {Query, Mutation, Resolver, Args} from "@nestjs/graphql"
import {PrismaService} from "../prisma/prisma.service"

@Resolver()
export class MessagesResolver {
    constructor(private readonly prisma: PrismaService){}

    @Query()
    messages(@Args() args) {
        return this.prisma.query.messages(args); // Forwarding the query on to the Prisma Service.
    }
    @Mutation()
    createMessage(@Args() args) {
        return this.prisma.mutation.createMessage(args)
    }
    @Mutation()
    deleteMessage(@Args() args) {
        return this.prisma.mutation.deleteMessage(args)
    }
}