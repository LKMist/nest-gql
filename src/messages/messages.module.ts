import { Module } from '@nestjs/common';
import { PrismaModule } from 'src/prisma/prisma.module';
import { MessagesResolver } from './messages.resolver';

@Module({
    imports: [PrismaModule],
    providers: [MessagesResolver],
    exports: [MessagesResolver]
})
export class MessagesModule {}
